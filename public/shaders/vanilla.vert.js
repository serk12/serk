var vertCode = `
attribute vec3 coordinate;
attribute vec3 normal;

uniform mat4 projection, modelView, normalMatrix;
uniform vec3 color;

varying vec4 fragColor;


float matshin = 400.0;
// vec3 materialColor = vec3(0.7, 0.3, 0.5);
vec3 colorFocus = vec3(0.8, 0.8, 0.8);
vec3 ambientLight = vec3(0.2, 0.2, 0.2);
vec3 sourceLight = vec3(10.0, 10.0, 10.0);

vec3 Lambert(vec3 NormSCO, vec3 L, vec3 color)
{
  vec3 colRes = ambientLight * color;
  if (dot (L, NormSCO) > 0.0)
    colRes = colRes + colorFocus * color * dot (L, NormSCO);
  return (colRes);
}

vec3 Phong(vec3 NormSCO, vec3 L, vec4 vertSCO, vec3 color)
{
  vec3 colRes = Lambert(NormSCO, L, color);
  if (dot(NormSCO,L) < 0.0)
    return colRes;

  vec3 R = reflect(-L, NormSCO);
  vec3 V = normalize(-vertSCO.xyz);

  if ((dot(R, V) < 0.0) || (matshin == 0.0))
    return colRes;
  float shine = pow(max(0.0, dot(R, V)), matshin);
  return (colRes + color * colorFocus * shine);
}

void main(void) {
  gl_Position = modelView * vec4(coordinate, 1.0);
  vec3 N = vec3(normalMatrix * vec4(normal, 0.0));
  vec3 L = normalize(sourceLight - coordinate).xyz;

  fragColor = vec4(Phong(N, L, vec4(coordinate, 1.0), color), 1.0);
}`;
