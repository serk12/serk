function mat4Identity(a) {
  a.length = 16;
  for (var i = 0; i < 16; ++i) a[i] = 0.0;
  for (var i = 0; i < 4; ++i) a[i + i * 4] = 1.0;
}

function mat4Perspective(a, fov, aspect, zNear, zFar) {
  var f = 1.0 / Math.tan (fov/2.0 * (Math.PI / 180.0));
  mat4Identity(a);
  a[0] = f / aspect;
  a[1 * 4 + 1] = f;
  a[2 * 4 + 2] = (zFar + zNear)  / (zNear - zFar);
  a[3 * 4 + 2] = (2.0 * zFar * zNear) / (zNear - zFar);
  a[2 * 4 + 3] = -1.0;
  a[3 * 4 + 3] = 0.0;
}

function vec3Dot(a, b) {
  return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

function vec3Cross(a, b, res) {
  res[0] = a[1] * b[2]  -  b[1] * a[2];
  res[1] = a[2] * b[0]  -  b[2] * a[0];
  res[2] = a[0] * b[1]  -  b[0] * a[1];
}

function vec3Normalize(a) {
  var mag = Math.sqrt(a[0] * a[0]  +  a[1] * a[1]  +  a[2] * a[2]);
  a[0] /= mag; a[1] /= mag; a[2] /= mag;
}


function mat4LookAt(viewMatrix,
  eyeX, eyeY, eyeZ,
  centerX, centerY, centerZ,
  upX, upY, upZ) {

  var dir = new Float32Array(3);
  var right = new Float32Array(3);
  var up = new Float32Array(3);
  var eye = new Float32Array(3);

  up[0]=upX; up[1]=upY; up[2]=upZ;
  eye[0]=eyeX; eye[1]=eyeY; eye[2]=eyeZ;

  dir[0]=centerX-eyeX; dir[1]=centerY-eyeY; dir[2]=centerZ-eyeZ;
  vec3Normalize(dir);
  vec3Cross(dir,up,right);
  vec3Normalize(right);
  vec3Cross(right,dir,up);
  vec3Normalize(up);
  // first row
  viewMatrix[0]  = right[0];
  viewMatrix[4]  = right[1];
  viewMatrix[8]  = right[2];
  viewMatrix[12] = -vec3Dot(right, eye);
  // second row
  viewMatrix[1]  = up[0];
  viewMatrix[5]  = up[1];
  viewMatrix[9]  = up[2];
  viewMatrix[13] = -vec3Dot(up, eye);
  // third row
  viewMatrix[2]  = -dir[0];
  viewMatrix[6]  = -dir[1];
  viewMatrix[10] = -dir[2];
  viewMatrix[14] =  vec3Dot(dir, eye);
  // forth row
  viewMatrix[3]  = 0.0;
  viewMatrix[7]  = 0.0;
  viewMatrix[11] = 0.0;
  viewMatrix[15] = 1.0;
}


function mat4Transpose(a, transposed) {
  var t = 0;
  for (var i = 0; i < 4; ++i) {
    for (var j = 0; j < 4; ++j) {
      transposed[t++] = a[j * 4 + i];
    }
  }
}

function mat4Invert(m, inverse) {
  var inv = new Float32Array(16);
  inv[0] = m[5]*m[10]*m[15]-m[5]*m[11]*m[14]-m[9]*m[6]*m[15]+
           m[9]*m[7]*m[14]+m[13]*m[6]*m[11]-m[13]*m[7]*m[10];
  inv[4] = -m[4]*m[10]*m[15]+m[4]*m[11]*m[14]+m[8]*m[6]*m[15]-
           m[8]*m[7]*m[14]-m[12]*m[6]*m[11]+m[12]*m[7]*m[10];
  inv[8] = m[4]*m[9]*m[15]-m[4]*m[11]*m[13]-m[8]*m[5]*m[15]+
           m[8]*m[7]*m[13]+m[12]*m[5]*m[11]-m[12]*m[7]*m[9];
  inv[12]= -m[4]*m[9]*m[14]+m[4]*m[10]*m[13]+m[8]*m[5]*m[14]-
           m[8]*m[6]*m[13]-m[12]*m[5]*m[10]+m[12]*m[6]*m[9];
  inv[1] = -m[1]*m[10]*m[15]+m[1]*m[11]*m[14]+m[9]*m[2]*m[15]-
           m[9]*m[3]*m[14]-m[13]*m[2]*m[11]+m[13]*m[3]*m[10];
  inv[5] = m[0]*m[10]*m[15]-m[0]*m[11]*m[14]-m[8]*m[2]*m[15]+
           m[8]*m[3]*m[14]+m[12]*m[2]*m[11]-m[12]*m[3]*m[10];
  inv[9] = -m[0]*m[9]*m[15]+m[0]*m[11]*m[13]+m[8]*m[1]*m[15]-
           m[8]*m[3]*m[13]-m[12]*m[1]*m[11]+m[12]*m[3]*m[9];
  inv[13]= m[0]*m[9]*m[14]-m[0]*m[10]*m[13]-m[8]*m[1]*m[14]+
           m[8]*m[2]*m[13]+m[12]*m[1]*m[10]-m[12]*m[2]*m[9];
  inv[2] = m[1]*m[6]*m[15]-m[1]*m[7]*m[14]-m[5]*m[2]*m[15]+
           m[5]*m[3]*m[14]+m[13]*m[2]*m[7]-m[13]*m[3]*m[6];
  inv[6] = -m[0]*m[6]*m[15]+m[0]*m[7]*m[14]+m[4]*m[2]*m[15]-
           m[4]*m[3]*m[14]-m[12]*m[2]*m[7]+m[12]*m[3]*m[6];
  inv[10]= m[0]*m[5]*m[15]-m[0]*m[7]*m[13]-m[4]*m[1]*m[15]+
           m[4]*m[3]*m[13]+m[12]*m[1]*m[7]-m[12]*m[3]*m[5];
  inv[14]= -m[0]*m[5]*m[14]+m[0]*m[6]*m[13]+m[4]*m[1]*m[14]-
           m[4]*m[2]*m[13]-m[12]*m[1]*m[6]+m[12]*m[2]*m[5];
  inv[3] = -m[1]*m[6]*m[11]+m[1]*m[7]*m[10]+m[5]*m[2]*m[11]-
           m[5]*m[3]*m[10]-m[9]*m[2]*m[7]+m[9]*m[3]*m[6];
  inv[7] = m[0]*m[6]*m[11]-m[0]*m[7]*m[10]-m[4]*m[2]*m[11]+
           m[4]*m[3]*m[10]+m[8]*m[2]*m[7]-m[8]*m[3]*m[6];
  inv[11]= -m[0]*m[5]*m[11]+m[0]*m[7]*m[9]+m[4]*m[1]*m[11]-
           m[4]*m[3]*m[9]-m[8]*m[1]*m[7]+m[8]*m[3]*m[5];
  inv[15]= m[0]*m[5]*m[10]-m[0]*m[6]*m[9]-m[4]*m[1]*m[10]+
           m[4]*m[2]*m[9]+m[8]*m[1]*m[6]-m[8]*m[2]*m[5];

  var det = m[0]*inv[0]+m[1]*inv[4]+m[2]*inv[8]+m[3]*inv[12];
  if (det == 0) return false;
  det = 1.0 / det;
  for (var i = 0; i < 16; i++) inverse[i] = inv[i] * det;
  return true;
}

var projection = new Float32Array(16);
this.resize = function (x, y, w, h) {
  gl.viewport(x, y, w, h);
  // this function replaces gluPerspective
  mat4Perspective(projection, 32.0, w/h, 0.5, 4.0);
  //mat4Print(projection);
}

/*============== Creating a canvas ====================*/
var canvas = document.getElementById('canvas');
gl = canvas.getContext('webgl');

/*======== Defining and storing the geometry ===========*/

var vertices = [
   -0.5,0.5,0.0,  1.0,0.0,0.0,
   -0.5,-0.5,0.0, 0.0,1.0,0.0,
   0.5,-0.5,0.0,  0.0,0.0,1.0,
];

indices = [0,1,2];

// Create an empty buffer object to store vertex buffer
var vertex_buffer = gl.createBuffer();
// Bind appropriate array buffer to it
gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
// Pass the vertex data to the buffer
gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
// Unbind the buffer
gl.bindBuffer(gl.ARRAY_BUFFER, null);

// Create an empty buffer object to store Index buffer
var index_Buffer = gl.createBuffer();
// Bind appropriate array buffer to it
gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_Buffer);
// Pass the vertex data to the buffer
gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

// Unbind the buffer
gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

/*================ Shaders ====================*/

// Create a vertex shader object
var vertShader = gl.createShader(gl.VERTEX_SHADER);

// Attach vertex shader source code
gl.shaderSource(vertShader, vertCode);

// Compile the vertex shader
gl.compileShader(vertShader);

// Create fragment shader object
var fragShader = gl.createShader(gl.FRAGMENT_SHADER);

// Attach fragment shader source code
gl.shaderSource(fragShader, fragCode);

// Compile the fragmentt shader
gl.compileShader(fragShader);

// Create a shader program object to store
// the combined shader program
var shaderProgram = gl.createProgram();

// Attach a vertex shader
gl.attachShader(shaderProgram, vertShader);

// Attach a fragment shader
gl.attachShader(shaderProgram, fragShader);

// Link both the programs
gl.linkProgram(shaderProgram);

// Use the combined shader program object
gl.useProgram(shaderProgram);

/*======= Associating shaders to buffer objects =======*/

// Bind vertex buffer object
gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);

// Bind index buffer object
gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_Buffer);

var coordinates = gl.getAttribLocation(shaderProgram, "coordinate");
gl.vertexAttribPointer(coordinates, 3, gl.FLOAT, false, 6 * 4, 0);
gl.enableVertexAttribArray(coordinates);

var normals = gl.getAttribLocation(shaderProgram, "normal");
gl.vertexAttribPointer(normals, 3, gl.FLOAT, false, 6 * 4, 3 * 4);
gl.enableVertexAttribArray(normals);


/*======= Associating uniforms =======*/
var modelViewLoc = gl.getUniformLocation(shaderProgram, "modelView");
var modelView = new Float32Array(16);

var projectionLoc = gl.getUniformLocation(shaderProgram, "projection");

var normalMatrixLoc = gl.getUniformLocation(shaderProgram, "normalMatrix");
var normalMatrix = new Float32Array(16);

var colorLoc = gl.getUniformLocation(shaderProgram, "color");


var t = 0.0;
function paint() {
  gl.clearColor(0.5, 0.5, 0.5, 0.9);
  gl.clear(gl.COLOR_BUFFER_BIT);

  var rect = canvas.getBoundingClientRect();
  this.resize(0, 0, rect.width, rect.height);


  t += 1.0;
  var rad = Math.PI / 180.0 * this.t;
  mat4LookAt(modelView,
             0.0, 0.0, 0.7, // eye
            -0.5,-0.5,0.0, // look at
             0.0, 0.0, 1.0); // up

  var modelViewInv = new Float32Array(16);
  mat4Invert(modelView, modelViewInv);
  mat4Transpose(modelViewInv, normalMatrix);

  gl.useProgram(shaderProgram);
  gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_Buffer);
  gl.uniformMatrix4fv(projectionLoc, false, projection);
  gl.uniformMatrix4fv(modelViewLoc, false, modelView);
  gl.uniformMatrix4fv(normalMatrixLoc, false, normalMatrix);
  var tCent = t / 100.0;
  var upOrDown = Math.floor(tCent) % 2 == 0;
  var blue = upOrDown? tCent - Math.floor(tCent) : 1.0 - (tCent - Math.floor(tCent));
  gl.uniform3f(colorLoc, Math.sin(rad), Math.cos(rad), blue);


  gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT,0);
}

setInterval(paint, 40);
